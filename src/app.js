import express from "express";
import bodyParser from "body-parser";
import session from "express-session";
import http from "http";

import cors from "cors";
import passport from "passport";

//config
import {
  successHandler,
  errorHandler as MoragnerrorHandler,
} from "./Config/morgan";
import config from "./Config/config";

//passport
import { jwtStrategy } from "./Config/passport";

//error handling
import { errorConverter, errorHandler } from "./Middleware/error";

//routes
import router from "./Routes/router";

import dotenv from "dotenv";
import "dotenv/config";

const app = express();
const server = http.Server(app);

if (config.env !== "test") {
  app.use(successHandler);
  app.use(MoragnerrorHandler);
}

app.use(bodyParser.json({ limit: "50mb" }));
app.use(session({ secret: "my_secret" }));

app.use(cors());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Authorization, Origin, Content-Type, Accept, Upload-Meta-Header"
  );
  res.setHeader("Access-Control-Allow-Credentials", "true");
  // res.setHeader("Upload-Meta-Header", "");

  next();
});

// // jwt authentication
// app.use(passport.initialize());
// passport.use("jwt", jwtStrategy);

// convert error to AppError, if needed
app.use(errorConverter);

// handle error
app.use(errorHandler);

app.use(router);

export { app, server };
