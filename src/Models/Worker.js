import mongoose from "mongoose";
import { omit, pick } from "lodash";
// import bcrypt from "bcrypt";

const Schema = mongoose.Schema;

const actionSchema = new Schema(
  {
    type: String,
    arguments: [String],
    success: Boolean,
    step: Number,
  },
  { _id: false }
);

const workerDetailSchema = new Schema(
  {
    actions: [actionSchema],
    capacity: String,
    holding: Number,
    name: String,
    location: String,
    nextAction: actionSchema,
    notificationUri: String,
    weight: Number,
  },
  { _id: false }
);

// Create Schema
const workerSchema = new Schema(
  {
    worker: [workerDetailSchema],
    url: String,
  },
  {
    timestamps: true,
  }
);

workerSchema.methods.transform = function () {
  let file = this;
  //   file.populate("uploadedBy");

  //   file.uploadedBy = file.uploadedBy
  //     ? file.uploadedBy.transform()
  //     : file.uploadedBy;
  //   file.files = file.files.map((data) => data.transform());

  return pick(file.toJSON(), ["_id", "worker", "url", "createdAt"]);
};

const worker = mongoose.model("worker", workerSchema);

export default worker;
