import mongoose from "mongoose";
import { omit, pick } from "lodash";
// import bcrypt from "bcrypt";

const Schema = mongoose.Schema;

// Create Schema
const workerOrderSchema = new Schema(
  {
    worker: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "worker",
    },
    orders: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "orders",
    },
  },
  {
    timestamps: true,
  }
);

workerOrderSchema.methods.transform = function () {
  let file = this;
  //   file.populate("uploadedBy");

  //   file.uploadedBy = file.uploadedBy
  //     ? file.uploadedBy.transform()
  //     : file.uploadedBy;
  //   file.files = file.files.map((data) => data.transform());

  return pick(file.toJSON(), ["_id", "worker", "url", "createdAt"]);
};

const worker = mongoose.model("workerorder", workerOrderSchema);

export default worker;
