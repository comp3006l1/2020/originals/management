import express from "express";

//controllers
import {
  initManagment,
  getOrders,
  getSelectedOrder,
  updateSelectedOrderData,
} from "../Controller/managmentController";

//middleware
import validate from "../Middleware/validate";
import auth from "../Middleware/auth";

const router = express.Router();

router.get("/init", initManagment);
router.get("/orders", getOrders);
router.get("/orders/:id", getSelectedOrder);
router.post("/orders/:id", updateSelectedOrderData);

export default router;
