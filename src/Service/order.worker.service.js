import httpStatus from "http-status";
import bcrypt from "bcryptjs";
import moment from "moment";

import config from "../Config/config";

//utils
import AppError from "../Utils/AppError";
import { sendGetRequest } from "../Utils/APIHandler";

//models
import OrderModel from "../Models/Orders";
import WorkerModel from "../Models/Worker";
import WorkerOrderModel from "../Models/WorkerOrder";

//services
import { sendAssignJobToWorker } from "./worker.service";
import { updateOrderStatus } from "./order.service";

/**
 *
 * @returns {} all orders
 */
export const asignWorkers = async () => {
  const orders = await OrderModel.find({ status: "NEW" });
  const workers = await OrderModel.find();

  await Promise.all(
    orders.map(async (data) => {
      const leastWorker = await getWorkerWithLeastOrders();
      console.log("test--- leastWorker", leastWorker);

      const duplicate = await WorkerOrderModel.findOne({
        worker: leastWorker.id,
        orders: data.id,
      });
      console.log("duplicate - WorkerOrderModel", duplicate);
      if (duplicate) return;

      if (!data.id) return;

      const createdWorker = await WorkerOrderModel.create({
        worker: leastWorker.id,
        orders: data.id,
      });

      await OrderModel.findByIdAndUpdate(data.id, {
        status: "PROCESSING",
      }).exec();
    })
  );
};

/**
 *
 * @returns {} all orders
 */
export const getWorkerWithLeastOrders = async () => {
  const workers = await WorkerModel.find();

  let workerArray = await Promise.all(
    workers.map(async (data) => {
      const workersOrders = await WorkerOrderModel.find({ worker: data._id });

      return {
        id: data._id,
        value: workersOrders.length,
      };
    })
  );

  console.log("workerArray", workerArray);

  workerArray.sort((a, b) => (a.value > b.value ? 1 : -1));

  return workerArray[0];
};

/**
 *send order to worker
 *
 * @returns item list
 */
export const assignJobToWorker = async () => {
  const WorkerOrder = await WorkerOrderModel.find()
    .populate("worker")
    .populate("orders")
    .lean();

  console.log("WorkerOrder------------", WorkerOrder);

  if (WorkerOrder.length <= 0) return;

  const orders = WorkerOrder.filter((data) => {
    if (!data.orders.isCompleted) return true;
    else return false;
  });

  const body = {
    ...orders[0].orders,
  };

  delete body._id;
  delete body.createdAt;
  delete body.updatedAt;
  delete body.__v;
  delete body.isCompleted;

  console.log("sending to worker----", body, orders[0].worker.url);

  await sendAssignJobToWorker(orders[0].worker.url, body);

  await updateOrderStatus("PROCESSING", orders[0].orders.id);
};

/**
 *
 * @returns {} all orders
 */
export const getAllOrders = async () => {
  const WorkerOrder = await WorkerOrderModel.find()
    .populate("worker")
    .populate("orders")
    .lean();

  return WorkerOrder;
};

/**
 *
 * @returns {} selected orders
 */
export const getSelectedOrders = async (id) => {
  const worker = await WorkerModel.findOne({ "worker.name": id });
  const workerOrder = await WorkerOrderModel.find({
    worker: worker._id,
  })
    .populate("worker")
    .populate("orders")
    .lean();

  console.log("worker -- ", id, workerOrder, worker);

  return workerOrder;
};

/**
 *
 * @returns {} selected orders
 */
export const updateOrderData = async (id) => {
  const WorkerOrder = await WorkerOrderModel.findOneAndUpdate(
    { orders: id },
    { status: "COMPLETED", isCompleted: true }
  );

  const order = await OrderModel.findOneAndUpdate(
    { orders: id },
    { status: "COMPLETED", isCompleted: true }
  );

  return WorkerOrder;
};
