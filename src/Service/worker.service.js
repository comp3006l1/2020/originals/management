import httpStatus from "http-status";
import bcrypt from "bcryptjs";
import moment from "moment";

import config from "../Config/config";

//utils
import AppError from "../Utils/AppError";
import { sendGetRequest, sendPostRequest } from "../Utils/APIHandler";

//models
import WorkerModel from "../Models/Worker";

//services

/**
 *Get Item List
 *
 * @returns item list
 */
export const getWorkerList = async () => {
  const API_ENDPOINT = config.worker + "/workers";

  const apiRespose = await sendGetRequest({
    url: API_ENDPOINT,
  });

  return apiRespose;
};

/**
 *Get Item List
 *
 * @returns item list
 */
export const sendAssignJobToWorker = async (url, body) => {
  const API_ENDPOINT = url;

  console.log("req body", body);

  const apiRespose = await sendPostRequest({
    url: API_ENDPOINT,
    body: { order: body },
  });

  return apiRespose;
};

/**
 *
 * @returns {} all orders
 */
export const saveWorkers = async (workers) => {
  console.log("workers--serv", workers);

  await Promise.all(
    workers.map(async (data) => {
      const duplicate = await WorkerModel.findOne({ url: data.url });
      console.log("duplicate - worker", duplicate);
      if (duplicate) return;

      const createdWorker = await WorkerModel.create(data);

      return createdWorker;
    })
  );
};
