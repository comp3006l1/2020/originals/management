import httpStatus from "http-status";
import bcrypt from "bcryptjs";
import moment from "moment";

import config from "../Config/config";

//utils
import AppError from "../Utils/AppError";
import { sendGetRequest, sendPutRequest } from "../Utils/APIHandler";

//models
import OrderModel from "../Models/Orders";

//itemList
import ItemList from "../../ItemList.json";

//services

/**
 *Get Item List
 *
 * @returns item list
 */
export const getOrderList = async () => {
  const API_ENDPOINT = config.order + "/orders";

  const apiRespose = await sendGetRequest({
    url: API_ENDPOINT,
  });

  return apiRespose;
};

/**
 *Get Item List
 *
 * @returns item list
 */
export const updateOrderStatus = async (status, id) => {
  const API_ENDPOINT = config.order + "/orders/" + id;

  const apiRespose = await sendPutRequest({
    url: API_ENDPOINT,
    body: {
      status,
    },
  });

  return apiRespose;
};

/**
 *
 * @returns {} all orders
 */
export const saveOrders = async (orderlist) => {
  await Promise.all(
    orderlist.map(async (data) => {
      const duplicate = await OrderModel.findById(data.id);
      if (duplicate) return;

      const createdOrder = await OrderModel.create({
        ...data,
        _id: data.id,
      });

      return createdOrder;
    })
  );
};

/**
 *
 * @returns {} all orders
 */
export const fetchAllOrders = async () => {
  const orders = await OrderModel.find().lean();

  return orders;
};

/**
 *
 * @returns {} all orders
 */
export const fetchOrdersByID = async (id) => {
  const orders = await OrderModel.findById(id).lean();

  return orders;
};

/**
 *
 * @returns {} all orders
 */
export const updateOrdersByID = async (id, status) => {
  const orders = await OrderModel.findByIdAndUpdate(id, { status });

  return orders;
};
