import passport from "passport";
import httpStatus from "http-status";

import AppError from "../Utils/AppError";

const verifyCallback = (req, resolve, reject, requiredRights) => async (
  err,
  user,
  info
) => {
  if (err || info || !user) {
    return reject(new AppError(httpStatus.UNAUTHORIZED, "Please authenticate"));
  }
  req.user = user;

  resolve();
};

const auth = (...requiredRights) => async (req, res, next) => {
  return new Promise((resolve, reject) => {
    passport.authenticate(
      "jwt",
      { session: false },
      verifyCallback(req, resolve, reject, requiredRights)
    )(req, res, next);
  })
    .then(() => next())
    .catch((err) => next(err));
};

module.exports = auth;
