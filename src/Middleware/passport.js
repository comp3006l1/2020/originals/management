import passport from "passport";
import { Strategy as GoogleTokenStrategy } from "passport-google-token";

import config from "../Config/config";

// GOOGLE STRATEGY
const GoogleTokenStrategyCallback = (
  accessToken,
  refreshToken,
  profile,
  done
) =>
  done(null, {
    accessToken,
    refreshToken,
    profile,
  });

passport.use(
  new GoogleTokenStrategy(
    {
      clientID: config.google.clientID,
      clientSecret: config.google.secret,
    },
    GoogleTokenStrategyCallback
  )
);

// promisified authenticate functions
const authenticateGoogle = (req, res) =>
  new Promise((resolve, reject) => {
    passport.authenticate(
      "google-token",
      { session: false },
      (err, data, info) => {
        if (err) reject(err);
        resolve({ data, info });
      }
    )(req, res);
  });

export { authenticateGoogle };
