import httpStatus from "http-status";

//utils
import catchAsync from "../Utils/CatchAsync";

//service
import {
  getOrderList,
  saveOrders,
  updateOrderStatus,
} from "../Service/order.service";
import { getWorkerList, saveWorkers } from "../Service/worker.service";
import {
  asignWorkers,
  assignJobToWorker,
  getAllOrders,
  getSelectedOrders,
  updateOrderData,
} from "../Service/order.worker.service";

export const initManagment = catchAsync(async (req, res) => {
  console.log("step counter working", req.body);

  const order = await getOrderList();
  await saveOrders(order.orders);

  const workers = await getWorkerList();
  await saveWorkers(workers.data);

  await asignWorkers();

  await assignJobToWorker();

  const response = { success: true };
  res.status(httpStatus.CREATED).send(response);

  const interval = setInterval(async function () {
    const order = await getOrderList();
    await saveOrders(order.orders);

    const workers = await getWorkerList();
    await saveWorkers(workers.data);

    await asignWorkers();
  }, 5000);

  // clearInterval(interval);
});

export const getOrders = catchAsync(async (req, res) => {
  console.log("step counter working", req.body);

  const orders = await getAllOrders();

  const response = { success: true, orders };
  res.status(httpStatus.CREATED).send(response);
});

export const getSelectedOrder = catchAsync(async (req, res) => {
  console.log("step counter working", req.body);

  const orders = await getSelectedOrders(req.params.id);

  const response = { success: true, orders };
  res.status(httpStatus.CREATED).send(response);
});

export const updateSelectedOrderData = catchAsync(async (req, res) => {
  console.log("step counter working", req.body);

  const orders = await updateOrderData(req.params.id);

  await updateOrderStatus("COMPLETED", req.params.id);

  await assignJobToWorker();

  const response = { success: true, orders };
  res.status(httpStatus.CREATED).send(response);
});
