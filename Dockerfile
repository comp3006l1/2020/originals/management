FROM node:13-alpine

RUN mkdir -p /usr/src/node-app

WORKDIR /usr/src/node-app

#Adding relevant folders to image

COPY package.json yarn.lock ./



COPY . .

RUN yarn install 

RUN yarn build

EXPOSE 3000
